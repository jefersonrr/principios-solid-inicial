/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LSP.Ejemplo2;

/**
 *
 * @author USUARIO
 */
public class DispositivoDeTransporte {
    
    String nombre;
    Double velocidad;
    Motor motor;

    public DispositivoDeTransporte(String nombre, Double velocidad, Motor motor) {
        this.nombre = nombre;
        this.velocidad = velocidad;
        this.motor = motor;
    }
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(Double velocidad) {
        this.velocidad = velocidad;
    }

    public Motor getMotor() {
        return motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }
    
    public void encenderMotor(){
    ///Hace algo
    }
    
}
